# Exercise 3: ML Lifecycle Case Study

[App8 Case Study](https://aws.amazon.com/solutions/case-studies/app-8/)

Written response, select 3 of the 11 steps and write a short paragraph explaining how you would go about executing on these parts of the ML Lifecycle.

Exact solutions will vary but should be somewhat related to the answers below.

## Problem
As a result of the COVID19 pandemic, no contact dining became a main priority for restaurants and business owners needed a contactless experience that would improve the customer experience and optimize restaurant staff time. With App8, its own proprietary software had a severely greater overhead as compared to the use of AWS tools and transitioning added to efficiency

## Business Objective
App8 wanted to develop a tool to predict customer volume and menu item demand to optimize profit margin, mitigate waste, and optimize staff scheduling

‘With this tool, you can actually predict how many people are going to come to your restaurant at a specific time and how many menu items are going to be in demand. This will enable you to better manage your inventory’

## Model
App8 using AWS tools created 3 models that can allow for optimal results for each business case: P10, P50, P90 ranging from a model for restaurants that typically overstock, restaurants that overstock and understock in equal margins, and restaurants that miss customer demand (understaffing). At an added benefit, business owners can run ‘what-if’ scenarios in times of uncertainty in order to deliver worthwhile forecasts 
